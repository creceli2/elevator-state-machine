#include "stdafx.h"
#include "Floor.h"
#include "Controller.h"


CFloor::CFloor()
{
}


CFloor::~CFloor()
{
}

void CFloor::SetUp(bool status)
{
	mUp = status;
	mController->SetCallLight(mFloor, CElevatorController::Up, mUp);
}

void CFloor::SetPanel(bool panel)
{
	mPanel = panel;
	mController->SetPanelFloorLight(mFloor, mPanel);
}

void CFloor::SetDown(bool status) 
{
	mDown = status;
	mController->SetCallLight(mFloor, CElevatorController::Down, mDown);
}
