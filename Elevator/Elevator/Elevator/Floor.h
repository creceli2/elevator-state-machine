#pragma once

class CController;

///Class to create the floor for every floor of the elevator
class CFloor
{
public:
	///Constructor
	CFloor();

	///Destructor
	virtual ~CFloor();

	/** Sets a controller to the floor of a building
	*
	* \param cont Controller to set
	*/
	void SetController(CController * cont) { mController = cont; }

	/** Sets the floor the elevator is on to the member variable for floor
	*
	* \param floor Floor to set as current
	*/
	void SetFloor(int floor) { mFloor = floor; }

	/** Function to return the current floor for the elevator
	*
	* \return mFloor Floor were currently at the elevator
	*/
	int GetFloor() { return mFloor; }
	
	/** Getter for which direction the called floor is reletive to the elevator
	*
	* \return mUp Boolean for which direction the new floor is
	*/
	bool IsUp() { return mUp; }

	/** Setter for the direciton of the called floor relative to the elevator
	*
	* \param status Status for the direciton
	*/
	void SetUp(bool status);


	/** Getter for if the called floor is below the car
	* \return mDown Boolean return for if the car is below the floor or not
	*/
	bool IsDown() { return mDown; }

	/** Setter for the direciton of the floor relative to the car
	*
	* \param status Status of the direction
	*/
	void SetDown(bool status);

	/** Getter for the curent panel of buttons inside the elevator car
	*
	* \return mPanel  Returns the panel of buttons in the elevator
	*/
	bool GetPanel() { return mPanel; }

	/** Sets the working panel to the working elevator car
	*
	* \param panel Panel to set to the car
	*/
	void SetPanel(bool panel);


private:

	///the controller for the elevator
	CController *mController = nullptr;   ///< Controller for this object

	///if the elevator is moving up
	bool mUp = false;

	///if the elevator is moving down
	bool mDown = false;

	///the controll panel for the elevator
	bool mPanel = false;

	///the floor the elevatory is on
	int mFloor = 0;
};

