/**
 * \file Controller.h
 *
 * \author William Crecelius
 *
 * Creates the controller class for the elevator
 */

#pragma once
#include "ElevatorController.h"
#include "Floor.h"

///Derives the Controller from the CElevatorController class
class CController : public CElevatorController
{
public:
	///Constructor
	CController();

	///Destructor
	virtual ~CController();

	/** Handler for when the Open Buttoen is pressed
	*/
	virtual void OnOpenPressed() override;

	/** Handler for when the Open Buttoen is pressed
	*/
	virtual void OnClosePressed() override;

	/// The state machine states
	enum States { Idle, DoorOpening, DoorOpen, DoorClosing, Moving, Stop};
	
	/** Setter for the different states for the Elevator
	*
	* \param state State to set the elevator at
	*/
	void SetState(States state);

	/** Operation for servicing different floor dependent on the state
	* and other buttons pressed
	*/
	virtual void Service() override;

	/** When a button on the elevator panel is pressed
	*
	* \param panel Panel to set the elevator
	*/
	virtual void OnPanelFloorPressed(int panel) override;

	/** Function handler for when the UpButton is pressed on a floor
	*
	* \param floor Floor that called the elevator
	*/
	void OnCallUpPressed(int floor) override;

	/** Function handler for when the DownButton is pressed on a floor
	*
	* \param floor Floor that called the elevator
	*/
	void OnCallDownPressed(int floor) override;

	/**
	 * Determine the floor to go to.
	 *
	 * Given the current direction we are going, determine what floor
	 * we should go to.
	 * \returns A floor to go to (1 to 3) or 0 if none
	 */
	int WhatFloorToGoTo();

	/** Determines a floor
	* \returns the floor to go to if youre already going up
	*/
	int WhatFloorUp();

	/** Determines a floor
	* \returns the floor to go to if youre already going down
	*/
	int WhatFloorDown();


private:
	int mFloor = 1;      ///< The current floor

	States mState = Idle;   ///< The current state

	double mStateTime = 0;  ///< The time in a current state

	/// An object for each floor
	CFloor mFloors[NumFloors];

	///telling the direction the elevator is moving
	bool mGoingUp = true;
};

